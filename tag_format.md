# Internet Attribution Project: Tag Format Proposal

Last updated: 2023-04-02 (maybe)

A proposal for an attribution tag format for Internet content to help improve
attribution of works, especially creative content.  For Site and Field
definition formats for use in internet_attribution.js, see
[ia_site_field_format.md](ia_site_field_format.md).

## Examples

- [a:ramoflo]
- [a:wallace.wells]
- [l:Toronto, Canada]
- [p:instagram]
- [d.taken:2023-01-01 12:34:56]
- [a:ScottP][p:instagram][d:2012-01-14][t:Show at the Roxy][l:Roxy, Toronto]

## Basic Tag Format

The goal is to be easy to parse and consistent. Individual tags will be enclosed
with '[' and '].  They will have a key and a value, separated by a ':'.  Keys
can have a primary and a secondary portion separated by a '.'.

- [&lt;primary>:&lt;value>]
- [&lt;primary>.&lt;secondary>:value]

### Decisions to make

Decisions need to be made about how to handle the presence of special characters
within values.

In theory tag names (keys) can be prescribed to avoid issues
(e.g. no colons or square brackets).

Colons in values could be acceptable (i.e.: only the first colon in a tag
delimits the key from the value and any subsequent ones are acceptable in the
value.)

But what about square brackets?
- Do we allow them if people enclose values with double-quotes? (And then make
  double-quotes within the value double up like CSV?)
- Or use backslashes to escape them (running into fun issues when coding rules
  in programming languages on how many backslashes you need to escape)?
- Or require a substitution, like HTML entities (then deal with issues encoding them for HTML)

For now, I recommend:
- avoid using any square brackets within values, but if you need to encode them,
  use UTF entity names, like "&amp;lsqb;" for '[' and "&amp;rsqb;" for ']'
  (though then you have to escape the & if you're displaying in HTML).

## Proposed tags 

The following list should probably be revised for better specificity.  We can
outline a date format, but what would the location format be?  Also, there's
ambiguity in meaning, for 's' and 'a' especially and my guidance for 'p'.
During testing, the tags I'm most interested in have been author (a), date (d)
and platform (p).

Also, most proposed tags are a single character for conciseness, but perhaps we
could do with an "[id:" tag for documenting numeric identifiers for some content.

- a: author, possibly also 'artist'.  Value would normally be a username for an account.
- d: date.  Value should be in an ISO8601-like format. YYYY-mm-dd, or YYYY-mm-ddTHH:MM:SS.sssT
    - d.&lt;specific_type>, e.g. d.posted, d.taken
- l: location.  
- s: series, possibly also 'set', could be used for a subreddit.  If there's two type, perhaps be specific
    - "[s.subreddit:scottpilgrim]" vs. "[s.series:Scott Pilgrim]"
- p: platform, value may be the domain name, like instagram.com, or just
  instagram if it's very notable (?), or have a section so instagram.stories.
    - p:&lt;platform>.&lt;section>, e.g. instagram.stories
- t: title, or possibly some descriptive tag.
- c: "characters", e.g. the people/subjects in the unit of media, could go well with 's' as series
- u: URL, or possibly URI
- x: extension, intended to be used with a secondary key, 
    - e.g. "[x.flavour:sweet]" for tagging photos of apples.

## Alternatives

I am using this currently as a fun way to cite content for various projects, but
in case anyone else gets interested, we should consider what systems already exist
that do the same thing.   E.g. the MARC 21 format for bibliographic data and popular 
academic citation formats overlap.
