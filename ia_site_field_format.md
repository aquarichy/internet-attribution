# Site and Fields definitions format

Last updated: 2023-04-02 (maybe)

Outlines the Site definition and Field definition formats used in internet_attribution.js.

For an explanation of the proposed tag format that these definitions support, see [tag_format.md](tag_format.md). An example tag with multiple fields would be: "[a:ScottP][p:instagram][d:2012-01-14][t:Show at the Roxy][l:Roxy, Toronto]".


## Example Site and Fields definition

<pre>
let sites = [
  /* old.reddit.com/r/SUBREDDIT/* */
  {
    "url_re":      /^https:\/\/old\.reddit\.com\/r\/[^\/]+\//,
    "article_sel": "DIV.content > DIV.sitetable > DIV.thing",
    "platform":    "reddit.com",
    
    "fields": [
      { "tag": "a", "name": "usernames", "sel": "a.author" },
      { "tag": "d", "name": "date",      "sel": "time" },
      { "tag": "s", "name": "series",
        "func": function () { return window.location.href.split ("/")[4]; } },
    ],

    "styles": {
      "attr_panel": {
        "top": "20px"
      }
    }
  },
  ... 
]
</pre>

## Site definition JSON format

Currently, Site definitions exist in internet_attribution.js, in the 'sites' global variable.

Required values:

* **url_re**: a regular expression to match against the window.location.href URL
* **article_sel**: a CSS selector that matches articles, the primary form of content we want to set an attribution on
* **platform**: the name of the platform, for storage in a 'p' tag, e.g. "[p:tumblr]"
* **fields**: a list [] of Field definitions

Optional values:

* **media_sel**: a CSS selector that matches individual media items within an article, if we want to tag them
  each individually, rather than the whole article.
* **styles**: a set of style overrides that can be applied to various elements, such as:
    * attr_panel: override styles on the attribution panel in the top-right of the page, (DIV.attr_panel)
    * attach_elem: override style on the element we attach an attribution tag
      to, to better accommodate the tag (e.g. adding padding or relative
      position).<br />The attach element can differ from both the ARTICLE from
      "article_sel" and media items from "media_sel", e.g. if the 'parent_level'
      Site definition property option sees us attaching the tag upon a higher
      container.
* **parent_level**: when attaching an attribution tag, we may want to attach it to a
  parent container (e.g. to avoid covering some media); this sets how many levels up we will go.
* **from_body**: if set and 'true', fields from 'fields' will be looked up
  relative to the whole document BODY, rather than to individual articles;
  useful for the complex case of Instagram stories
* **mutation_observer**: a mutation observer can be set for complex cases like Instagram stories
  where content changes in one part of the page and the metadata appears in another. We want to observe
  the changes in one place to be able to update the metadata tag in another.  It has two properties:
    * **observee_sel**: a CSS selector for the object we will observe.
    * **observer**: a MutationObserver object that will do the observation.

## Field definition format

Currently, Field definitions exist within Site definitions in internet_attribution.js (see above).

Field properties:

* **tag**: Required. The tag's key, e.g. 'a' for author in a tag like "[a:Knives Chau]"
* **name**: Optional.  Descriptor of the field, useful for other developers.  E.g. 'usernames' for 'a' tag.
  It is currently unused beyond the definition, but could be useful for logging.
* **sel**: CSS selector to locate the field's corresponding element(s).
    * Value:
        * For most elements, their innerText will be used as their value 
        * For TIME elements, their datetime attribute will be used
        * If multiple elements are matched, the final value will be a ','-joined concatenation 
          of the individual value of each element.  E.g. "[a:scott,ramona]".
    * Presence:
        * Optional, if a 'func' function handles value retrieval
          without needing a 'sel' (e.g. the value is obtained from the page URL).
        * Required, if no func is defined.
* **func**: Optional. A JavaScript function that can be used to determine a
  value beyond the content of the element identified by 'sel'.
    * If 'sel' is also defined, the function will be passed each element from
      'sel' as its only argument element found by 'sel' to extract its value.
    * If 'sel' is not defined, a function can be defined to retrieve the field
      value through some other way, like via the page's URL.

We might possibly support a **value** property at some point, to allow a default value
in case 'sel' and 'func' fail to find something or are both absent.

Recommended fields:

* { 'tag': 'a', ... }
    * Identify the creator/artist/author of a work.
* { 'tag': 'd', ... }
    * Identify the time the work was posted/created
