/*** internet_attribution.js ***
 *
 * An extension to expose basic attribution metadata in a consistent tagged format.
 *
 * See for details:
 * https://gitlab.com/aquarichy/internet-attribution
 *
 * License: GPLv3
 * Copyright: Richard Schwarting, 2023
 */

/** Site Specific Functions/Variables **/

/* IG Stories *

   Instagram Stories are a bit weird.  Rather than having a containing element
   for both the media and metadata, they now have a shared HEADER element that they update
   depending on which media is being shared.
   
   They also have a double-carousel thing going on.  There's the outer one that
   rotates between users, and the inner one that rotates within Stories for a
   given user (from the past 24 hours).
*/
const ig_stories_mutation_observer = new MutationObserver ((recs) => {
  console.log ("mutate");
  
  for (let attred_elem of document.body.querySelectorAll (".attred")) {
    attred_elem.classList.remove ("attred");
  }
  for (let div of document.body.querySelectorAll ("div.attr_div")) {
    div.parentNode.removeChild (div);
  }
});
function ig_stories_get_username (username_elem) {
  if (window.location.href.match (/\/stories\/highlights\//)) {
    return username_elem.href.split ("/")[3];
  } else {
    return username_elem.innerText;
  }
}
function ig_stories_get_series (series_elem) {
  if (window.location.href.match (/\/stories\/highlights\//)) {
    return series_elem.innerText;
  } else {
    return null;
  }
}

/** Site Definitions **/

let sites = [
  /* old.reddit.com/r/SUBREDDIT/* */
  {
    "url_re":      /^https:\/\/old\.reddit\.com\/r\/[^\/]+\//,
    "article_sel": "DIV.content > DIV.sitetable > DIV.thing",
    "platform":    "reddit.com",
    
    "fields": [
      { "tag": "a", "name": "usernames", "sel": "a.author" },
      { "tag": "d", "name": "date",      "sel": "time" },
      { "tag": "s", "name": "series",
        "func": function () { return window.location.href.split ("/")[4]; } },
    ],

    "styles": {
      "attr_panel": {
        "top": "20px"
      }
    }
  },

  /* old.reddit.com main page* */
  {
    "url_re":      /^https:\/\/old\.reddit\.com\/?$/,
    "article_sel": "DIV.content > DIV.sitetable > DIV.thing",
    "platform":    "reddit.com",
    
    "fields": [
      { "tag": "a", "name": "usernames", "sel": "a.author" },
      { "tag": "s", "name": "series", "sel": "a.subreddit",
        "func": function (a) { return a.innerText.split ("/")[1]; } },
      { "tag": "d", "name": "date", "sel": "time" },
    ],

    /* override some style where we're attaching the attribution to make it fit  */
    "styles": {
      "attach_elem": {
        "position": "relative",
        "paddingTop": "2em"
      }
    }
  },

  /* Twitter: user feed */
  {
    /* Test Source: twitter.com/nintendouk */

    "url_re":      /^https:\/\/twitter\.com($|\/)/,
    "article_sel": "ARTICLE",
    "platform":    "twitter.com",

    "fields": [
      { "tag": "a", "name": "usernames",
        "sel": "DIV[data-testid=\"User-Name\"] > "+
               "DIV:first-child A.css-4rbku5.css-18t94o4.css-1dbjc4n.r-1loqt21.r-1wbh5a2.r-dnmrzs.r-1ny4l3l",
        "func": function (a) { return a.href.split ("/")[3]; }
      },
      { "tag": "d", "name": "date", "sel": "time" },
    ],

    "styles": {
      "attach_elem": {
        "paddingTop": "3em"
      }
    }
  },

  /* Instagram: single post page */
  {
    /* Test Source: instagram.com/nintendoamerica */

    "url_re":      /^https:\/\/www\.instagram\.com(\/(p|reel)\/[A-Za-z0-9-]+\/|\/?$)/,
    "article_sel": "ARTICLE._aatb._aate._aati",    
    /* if you want to attach to each media under an article */
    "media_sel": [ "VIDEO.x1lliihq.x5yr21d.xh8yej3",
                   "VIDEO._ab1d",
                   "IMG.x5yr21d.xu96u03.x10l6tqk.x13vifvy.x87ps6o.xh8yej3" ].join (", "),

    "parent_level": 1,

    "platform": "instagram.com",

    "fields": [
      { "tag": "a", "name": "usernames",
        "sel": "SPAN.x1lliihq.x1plvlek.xryxfnj.x1n2onr6.x193iq5w.xeuugli.x1fj9vlw.x13faqbe.x1vvkbs.x1s928wv.xhkezso.x1gmr53x.x1cpjm7i.x1fgarty.x1943h6x.x1i0vuye.xvs91rp.x1s688f.x5n08af.x10wh9bi.x1wdrske.x8viiok.x18hxmgj > "+
          "DIV.xt0psk2 > DIV.xt0psk2 > A"
      },
      { "tag": "d", "name": "date",
        "sel": [
          "DIV._ae5u._ae5v._ae5w TIME", /* per-post/reel pages, date appears at bottom */
          "DIV._aacl._aaco._aacu._aacy._aad6._aade TIME" /* feed page, date appears at top */
        ].join (", ")
      }
    ]
  },

  /* Instagram Stories */
  {
    "url_re":      /^https:\/\/www\.instagram\.com\/stories\/[^/]+\/[0-9]+\//,
    "article_sel": "VIDEO, IMG._aa63",
    "platform":    "instagram.com:stories",

    "fields": [
      { "tag": "a", "name": "usernames", "sel": "HEADER._ac0k A.x1i10hfl.xjbqb8w.x6umtig.x1b1mbwd.xaqea5y.xav7gou.x9f619.x1ypdohk.xt0psk2.xe8uvvx.xdj266r.x11i5rnm.xat24cr.x1mh8g0r.xexx8yu.x4uap5.x18d9i69.xkhd6sd.x16tdsg8.x1hl2dhg.xggy1nq.x1a2a7pz.notranslate._ac0s._a6hd",
                     "func": ig_stories_get_username },
      { "tag": "d", "name": "date", "sel": "HEADER._ac0k TIME._ac0t" },
      { "tag": "s", "name": "series", "sel": "HEADER._ac0k A.x1i10hfl.xjbqb8w.x6umtig.x1b1mbwd.xaqea5y.xav7gou.x9f619.x1ypdohk.xt0psk2.xe8uvvx.xdj266r.x11i5rnm.xat24cr.x1mh8g0r.xexx8yu.x4uap5.x18d9i69.xkhd6sd.x16tdsg8.x1hl2dhg.xggy1nq.x1a2a7pz.notranslate._ac0s._a6hd",
                     "func": ig_stories_get_series }
    ],

    /* Optional parametres */
    "mutation_observer": {
      /* IG Stories share one header for many media items, so we need to clear it when we change items */
      "observee_sel": "SECTION > DIV._ac0b",
      "observer":     ig_stories_mutation_observer,
    },

    "parent_level":   5, /* don't attach directly to the article (an IMG or VIDEO here), but 5 levels above */
    "from_body":      true, /* look up fields relative to body, not article */
  },

  /* Test Source: splatoonus.tumblr.com */
  {
    /* TODO: exclude www.tumblr.com */
    "url_re":      /^https:\/\/[^\.]+\.tumblr\.com($|\/)/,
    "article_sel": "ARTICLE",
    "platform":    "tumblr.com",

    "fields": [
      { "tag": "a", "name": "usernames",
        "func": function (a) { return window.location.href.split ("/")[2].split (".")[0]; } },
      { "tag": "d", "name": "date",      "sel": "TIME" },
    ],

    "styles": {
      "attr_panel": {
        "top": "60px"
      }
    }
  }
];

/** General Functions **/

function get_field (article, site, field) {
  let values = [];
  let elems = [];

  if (field.sel) {
    elems = (site.from_body ? document.body : article).querySelectorAll (field.sel);
  } else {
    elems = [ article ];
  }

  for (let elem of elems) {
    let value = null;

    if (field.func) {
      value = field.func (elem);
    } else if (elem.nodeName == "TIME") {
      value = elem.getAttribute ("datetime");
    } else {
      value = elem.innerText;
    }

    values.push (value);
  }

  const values_str = values.join (",");

  return values_str;
}

function get_attr (article, site) {
  let attrs = "";
  for (let field of site.fields) {
    const value = get_field (article, site, field);

    if (value) {
      attrs += `[${field.tag}:${value}]`;
    }
  }
  attrs += `[p:${site.platform}]`;

  return attrs;
}

function create_close_a (attr_div) {
  let close_a = document.createElement ("A");

  close_a.innerText = "×";
  close_a.onclick = function () {
    attr_div.parentNode.removeChild (attr_div);
  };

  Object.assign (close_a.style, {
    position: "absolute",
    top: "2px",
    right: "2px",
    lineHeight: "0.6em",
    border: "1px solid black",
    userSelect: "none",
    backgroundColor: "rgba(192,192,192,0.75)",
    color: "black"
  });

  return close_a;
}

function create_attr_div (attr) {
  let attr_div = document.createElement ("DIV");
  attr_div.classList.add ("attr_div");

  Object.assign (attr_div.style, {
    position: "absolute",
    top: "0",
    left: "0",
    backgroundColor: "rgba(200,200,200,0.5)",
    color: "black",
    border: "1px solid black",
    paddingRight: "1.5em",
    paddingLeft: "0.25em",
    userSelect: "all",
    zIndex: 1,

  });

  let attr_span = document.createElement ("SPAN");
  attr_span.classList.add ("attr_span");
  attr_span.innerText = attr;
  attr_div.appendChild (attr_span);

  let close_a = create_close_a (attr_div);
  attr_div.appendChild (close_a);

  return attr_div;
}

function attach (elem, site, attr) {
  let attr_div = create_attr_div (attr);

  if (site.parent_level) {
    for (let i = 0; i < site.parent_level; i++) {
      elem = elem.parentNode;
    }
  }

  if (site.styles && site.styles.attach_elem) {
    Object.assign (elem.style, site.styles.attach_elem);
  }

  elem.appendChild (attr_div);
}

function get_basename (media) {
  let src = media.getAttribute ("src");

  if (!src) {
    let source = media.querySelector ("SOURCE"); /* TODO: handle multiple */
    if (source) {
      src = source.getAttribute ("src");
    }
  }

  if (!src) {
    return null;
  }

  let url = src.split ("?")[0];
  let url_parts = url.split ("/");
  let basename = url_parts[url_parts.length - 1];

  return basename;
}

function parse_unit (article, site, subject, attr_appendix = "") {
  if (subject.classList.contains ("attred")) {
    return;
  }

  let attr = get_attr (article, site);
  attach (subject, site, attr + attr_appendix);
  subject.classList.add ("attred");
}

let prev_articles_len = -1;

function parse_site (site) {
  const articles = document.body.querySelectorAll (site.article_sel + ":not(.attred)");

  if (articles.length == 0) {
    if (prev_articles_len != 0) {
      console.log ("no articles found");
    }

    prev_articles_len = 0;
    return;
  }
  prev_articles_len = articles.length;

  for (let article of articles) {
    console.log ("found article");

    if (site.media_sel) {
      for (let media of article.querySelectorAll (site.media_sel + ":not(.attred)")) {
        let basename = get_basename (media);
        parse_unit (article, site, media, `[f:${basename}]`);
      }
    } else {
      parse_unit (article, site, article);
    }

    if (site.mutation_observer) {
      let observee = document.querySelector (site.mutation_observer.observee_sel);
      if (observee) {
        site.mutation_observer.observer.observe (observee, { "characterData": true, "subtree": true });
      } else {
        console.log ("ERROR: " + site.mutation_observer.observee_sel);
      }
    }
  }
}

function create_panel_button (text, onclick_func) {
  button_a = document.createElement ("A");
  button_a.textContent = text;
  button_a.onclick = onclick_func;

  Object.assign (button_a.style, {
    "border":          "1px solid black",
    "backgroundColor": "rgba(192,192,192,0.75)",
    "color":           "black",
    "marginLeft":      "0.5em",
    "display":         "inline-block",
    "textAlign":       "center",
    "width":           "6em"
  });

  return button_a;
}

function create_attr_panel (site) {
  let ia_div = document.querySelector ("div#attr_panel");
  if (ia_div) {
    /* replace if one already exists, for testing updates */
    ia_div.parentNode.removeChild (ia_div);
  }
  ia_div = document.createElement ("DIV");
  ia_div.id = "attr_panel";

  Object.assign (ia_div.style, {
    "position":     "fixed",
    "right":        "0px",
    "top":          "0px",
    "zIndex":       "101",
    "paddingTop":   "0.5em",
    "paddingRight": "1.5em"
  });

  if (site.styles && site.styles.attr_panel) {
    Object.assign (ia_div.style, site.styles.attr_panel);
  }

  const hide_all_a = create_panel_button ("Hide Attrs", function () {
    for (let div of document.querySelectorAll ("DIV.attr_div")) {
      if (div.style.display == "none") {
        div.style.display = "block";
        hide_all_a.textContent = "Hide Attrs";
      } else { 
        div.style.display = "none";
        hide_all_a.textContent = "Show Attrs";
      }
    }
  });

  const reset_all_a = create_panel_button ("Reset Attrs", function () {
    for (let div of document.querySelectorAll ("DIV.attr_div")) {
      div.parentNode.removeChild (div);
    }
    for (let elem of document.querySelectorAll (".attred")) {
      elem.classList.remove ("attred");
    }
  });

  const close_a = create_close_a (ia_div);

  ia_div.appendChild (hide_all_a);
  ia_div.appendChild (reset_all_a);
  ia_div.appendChild (close_a);

  return ia_div;
}




console.log ("internet_attribution.js");
for (let site of sites) {
  console.log (`site.url_re "${site.url_re}" vs "${window.location.href}"`);
  if (window.location.href.match (site.url_re)) {
    console.log (`site match: ${site.platform}`);

    const panel = create_attr_panel (site);
    document.body.appendChild (panel);

    /* keep checking every second, since many pages load as we scroll */
    setInterval (function () { parse_site (site); }, 1000);
    break;
  }
}
