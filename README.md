# Internet Attribution Project

## Attribution Tags

> <pre>[a:aquarichy][p:gitlab][d:2023-04-01][t:a README file]</pre>

See [tag_format.md](tag_format.md) for details.

## The problem

I see a lot of content shared across the Internet missing any attribution to its
original creator(s). E.g. people will share fan art they find online without
citing the artist, or share beautiful nature photos without citing the photographer.

One factor in missing attribution is that people will often screenshot content
to reshare it without attribution information being within frame. Others will
collect content they find online locally and then reshare it later, but won't
record attribution information with the file.

Another factor is that few sites attach attribution information directly to
files. If a friend posts some art to their Facebook, I am allowed to save that
locally, but no metadata comes with it. If an artist sets attribution metadata in
a file that they upload to many sites, it will be stripped out. That has
benefits for preserving privacy for people who don't actually want their content
to be attributable back to them, but prevents other creators from receiving credit.

Some sites seem to encourage this behaviour, like Pinterest, which encourages
users to collect media with very, very little concern for attribution.  Some
sites have functionality built in that allow people to re-share content within
the site, linking back to its creator, like Twitter and Tumblr, while others
don't.

## The goal

Make basic attribution data more accessible.  Initially, this project will
provide a Firefox extension that will surface an attribution tag containing
basic items like author, date and platform next to content online. The extension
will carry a list of site definitions that can be updated, which will define
per-site CSS selectors and JavaScript functions that allow us to grab the
relevant metadata from the page and surface it.

That way, if someone takes a screenshot, the attribution data can be visible
next to the content being screenshot. If someone is saving media locally, they
could copy the attribution tag and incorporate it into the file's filename.  I
don't plan on coding functionality to help download media with attribution data
included automatically in the filename - while that would be very helpful, many
sites make efforts to prevent saving content locally to protect a creator's
copyright, and this project is not seeking to undermine that.

Also, there could be room for advocacy, to encourage sites to incorporate
attribution data into files' metadata for platforms that allow downloading of
media.  Or persuade Pinterest to take copyright and attribution more seriously.

Both creators and users benefit when you can identify the name (and context) of
the media we enjoy.

## Examples

**Reddit**

![Reddit](examples/old.reddit.com.png)

**Instagram**

![Instagram](examples/instagram.png)

**Tumblr**

![Tumblr](examples/tumblr.png)

**Twitter**

![Twitter](examples/twitter.png)

## Usage

For now, I am just loading the extension locally.

To do so:

- go to "about:debugging#/runtime/this-firefox"
- click "temporarily load add-on..." (or whatever the English text is)
- navigate to where you saved the extension files and select manifest.js

Eventually, I'll upload it to Mozilla Add-ons

## Roadmap

- Add eslint support
- Extend the list of sites covered
- Add a configuration panel so people can choose which tags to show, and how they'll be displayed
- Move site definitions into their own JS
- Release an extension for Google Chrome
- Release an extension for mobile browsers (not that they support them much)
- Persuade other sites to improve support for attribution

## Contributing

Feel free to contribute new site definitions, update existing ones (CSS class names change often on ReactJS platforms and others), or new tools. If I am unresponsive or uncooperative, feel free to fork it!

## Authors and acknowledgment

- Richard Schwarting &lt;aquarichy@gmail.com&gt;

## License

GPLv3+

